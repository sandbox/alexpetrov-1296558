<?php
/**
 * @file
 * Provides infrequently used functions and hooks for external_block.
 */

/**
 * Implements hook_menu().
 */
function _external_block_menu() {
  $items['admin/structure/block/add-external-block'] = array(
    'title' => 'Add External HTML block',
    'description' => 'Add a new External HTML block.',
    'access arguments' => array('administer blocks'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('external_block_add_block_form'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'external_block.admin.inc',
  );
  $items['admin/structure/block/delete-external-block'] = array(
    'title' => 'Delete External HTML block block',
    'access arguments' => array('administer blocks'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('external_block_delete'),
    'type' => MENU_CALLBACK,
    'file' => 'external_block.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function _external_block_theme(&$existing, $type, $theme, $path) {
  // Add theme hook suggestion patterns for the core theme functions used in
  // this module. We can't add them during hook_theme_registry_alter() because
  // we will already have missed the opportunity for the theme engine's
  // theme_hook() to process the pattern. And we can't run the pattern ourselves
  // because we aren't given the type, theme and path in that hook.

  return array(
    'external_block_wrapper' => array(
      'template' => 'external-block-wrapper',
      'variables' => array('content' => array(), 'config' => array(), 'delta' => NULL),
      'pattern' => 'external_block_wrapper__',
    ),
  );
}

/**
 * Menu callback: display the external block addition form.
 *
 * @see external_block_add_block_form_submit()
 */
function external_block_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  return block_admin_configure($form, $form_state, 'external_block', NULL);
}

/**
 * Save the new external block.
 */
function external_block_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('external_block_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  variable_set('external_block_ids', $block_ids);

  // Save the block configuration.
  external_block_block_save($delta, $form_state['values']);

  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'region', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $region = !empty($form_state['values']['regions'][$theme->name]) ? $form_state['values']['regions'][$theme->name] : BLOCK_REGION_NONE;
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'status' => 0,
        'status' => (int) ($region != BLOCK_REGION_NONE),
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Alters the block admin form to add delete links next to external blocks.
 */
function _external_block_form_block_admin_display_form_alter(&$form, $form_state) {
  $blocks = module_invoke_all('external_block_blocks');
  foreach (variable_get('external_block_ids', array()) AS $delta) {
    if (empty($blocks[$delta])) {
      $form['blocks']['external_block_' . $delta]['delete'] = array('#type' => 'link', '#title' => t('delete'), '#href' => 'admin/structure/block/delete-external-block/' . $delta);
    }
  }
}

/**
 * Menu callback: confirm deletion of external blocks.
 */
function external_block_delete($form, &$form_state, $delta = 0) {
  $title = _external_block_format_title(external_block_get_config($delta));
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/structure/block', NULL, t('Delete'), t('Cancel'));
}

/**
 * Deletion of external blocks.
 */
function external_block_delete_submit($form, &$form_state) {
  // Remove the external block configuration variables.
  $delta = $form_state['values']['delta'];
  $block_ids = variable_get('external_block_ids', array());
  unset($block_ids[array_search($delta, $block_ids)]);
  sort($block_ids);
  variable_set('external_block_ids', $block_ids);
  variable_del("external_block_{$delta}_url");

  db_delete('block')
    ->condition('module', 'external_block')
    ->condition('delta', $delta)
    ->execute();
  db_delete('block_role')
    ->condition('module', 'external_block')
    ->condition('delta', $delta)
    ->execute();
  drupal_set_message(t('The block "%name" has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}

/**
 * Implements hook_block_info().
 */
function _external_block_block_info() {
  $blocks = array();
  $deltas = variable_get('external_block_ids', array());
  foreach (array_keys(module_invoke_all('external_block_blocks')) as $delta) {
    $deltas[] = $delta;
  }
  foreach ($deltas AS $delta) {
    $blocks[$delta]['info'] = _external_block_format_title(external_block_get_config($delta));
    //$blocks[$delta]['cache'] = DRUPAL_NO_CACHE;
  }
  return $blocks;
}

/**
 * Return the title of the block.
 *
 * @param $config
 *   array The configuration of the external block.
 * @return
 *   string The title of the block.
 */
function _external_block_format_title($config) {
    return $config['admin_title'];
}

/**
 * Implements hook_block_configure().
 */
function _external_block_block_configure($delta = '') {
  // Create a pseudo form state.
  $form_state = array('values' => external_block_get_config($delta));
  return external_block_configure_form(array(), $form_state);
}

/**
 * Returns the configuration form for a external HTML block.
 *
 * @param $form_state
 *   array An associated array of configuration options should be present in the
 *   'values' key. If none are given, default configuration is assumed.
 * @return
 *   array The form in Form API format.
 */
function external_block_configure_form($form, &$form_state) {
  $config = array();
  // Get the config from the form state.
  if (!empty($form_state['values'])) {
    $config = $form_state['values'];
  }
  // Merge in the default configuration.
  $config += external_block_get_config();

  // Build the standard form.
  $form['#attached']['css'][] = drupal_get_path('module', 'external_block') . '/external-block-admin.css';
  $form['#attached']['library'][] = array('system', 'ui.button');

  $form['external-block-wrapper-start'] = array(
    '#markup' => '<div id="external-block-settings">',
    '#weight' => -20,
  );
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $config['admin_title'],
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this block. If blank, the regular title will be used.'),
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#default_value' => $config['url'],
    '#title' => t('URL to External HTML'),
    '#description' => t('This can be either a local link inside drupal or link to external page that has HTML code'),
  );
  
  
  $form['external-block-wrapper-close'] = array('#markup' => '</div>');

  return $form;
}

/**
 * Implements hook_block_save().
 */
function _external_block_block_save($delta = '', $edit = array()) {
  if (!empty($delta)) {
    $config = external_block_get_config($delta);
    variable_set("external_block_{$delta}_url", $edit['url']);
    variable_set("external_block_{$delta}_admin_title", $edit['admin_title']);
  }
}

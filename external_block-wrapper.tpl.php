<?php
/**
 * @file
 * Default theme implementation to wrap external blocks.
 *
 * Available variables:
 * - $content: The renderable array containing the HTML.
 * - $classes: A string containing the CSS classes for the DIV tag. Includes:
 *   external-block-DELTA.
 * - $classes_array: An array containing each of the CSS classes.
 *
 * The following variables are provided for contextual information.
 * - $delta: (string) The external_block's block delta.
 * - $config: An array of the block's configuration settings. Includes
 *
 * @see template_preprocess_external_block_wrapper()
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($content); ?>
</div>
